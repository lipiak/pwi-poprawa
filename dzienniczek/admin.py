from django.contrib import admin
from models import User, Wpis, Tag, Grupa

admin.site.register(User)
#admin.site.register(Wpis)
admin.site.register(Tag)
admin.site.register(Grupa)

#@admin.register(Wpis)
class WpisAdmin(admin.ModelAdmin):
    list_filter = ('data',)
    list_display = ('data', 'autor', 'tresc', 'dataModyfikacji', 'autorModyfikacji',)
    search_fields = ('tagi__name',)
admin.site.register(Wpis, WpisAdmin)


# class WpisInline(admin.TabularInline):
#     model = Wpis
#     extra = 3
#     ordering = ('data', )
#
#
# class WpisAdmin(admin.ModelAdmin):
#     list_display = ('autor', 'tresc', 'data', )
#     list_filter = ('data', )
#     ordering = ('-data', )
#     inlines = (WpisInline, )
#     search_fields = ('tagi__name',)
#
# admin.site.register(Wpis, WpisAdmin)