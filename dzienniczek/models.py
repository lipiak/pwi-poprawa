from django.db import models

from django.utils import timezone

class Tag(models.Model):
    name = models.CharField(max_length=50)

    def __unicode__(self):
        return self.name

class Grupa(models.Model):
    nazwa = models.CharField(max_length=100)

    def __unicode__(self):
        return self.nazwa

class User(models.Model):
    login = models.CharField(max_length=100)
    haslo = models.CharField(max_length=100)
    email = models.EmailField()
    aktywowany = models.BooleanField(default=False)
    tokenAktywacyjny = models.CharField(max_length=250)
    grupy = models.ManyToManyField(Grupa, null=True, blank=True)
    canvas_password = models.CharField(max_length=100)
    canvas_session = models.DateTimeField('rozpoczecie sesji')

    def __unicode__(self):
        return unicode.format(u'{0} {1}', self.login, self.haslo)

class CanvasUser(models.Model):
    login = models.EmailField()
    password = models.CharField(max_length=100)
    session_time = models.DateTimeField('rozpoczecie sesji')

    def __unicode__(self):
        return unicode.format(u'{0} {1} {2}', self.login, self.password)

class Wpis(models.Model):
    autor = models.CharField(max_length=100)
    tresc = models.CharField('tresc wpisu', max_length=500)
    data = models.DateTimeField('data wpisu')
    dataModyfikacji = models.DateTimeField('data modfikacji', null=True, blank=True)
    autorModyfikacji = models.CharField(max_length=200, null=True, blank=True)
    tagi = models.ManyToManyField(Tag)

    def __unicode__(self):
        return unicode.format(u'{0} {1}: {2}', self.data, self.autor, self.tresc)







